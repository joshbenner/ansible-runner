import tornado.web


class AppError(tornado.web.HTTPError):
    pass
