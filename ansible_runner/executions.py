from typing import List
from abc import ABC, abstractmethod

TagList = List[str]


class Execution(object):
    def __init__(self, id=None, playbook: str=None, include_tags: TagList=None,
                 exclude_tags: TagList=None, limit: str='all', status='new'):
        self.id = id
        self.status = status
        self.playbook = playbook
        self.include_tags = include_tags or []
        self.exclude_tags = exclude_tags or []
        self.limit = limit


ExecutionList = List[Execution]


class ExecutionsRepository(ABC):

    @abstractmethod
    def save(self, execution: Execution) -> Execution:
        # Fake persistence.
        return execution

    @abstractmethod
    def find_all(self) -> ExecutionList:
        # Not implemented.
        return []
