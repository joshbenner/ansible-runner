import json

from ansible_runner import AppError
from ansible_runner.executions import Execution


def parse_json(raw: str) -> Execution:
    try:
        parsed = json.loads(raw)
    except json.JSONDecodeError as e:
        err = str(e).partition(': ')[2]
        raise AppError(reason='Invalid JSON: {}'.format(err))
    return Execution(
        playbook=parsed['playbook'],
        include_tags=parsed.get('include_tags'),
        exclude_tags=parsed.get('exclude_tags'),
        limit=parsed.get('limit')
    )


def build_json(execution: Execution) -> dict:
    return {
        'id': execution.id,
        'status': execution.status,
        'playbook': execution.playbook,
        'include_tags': execution.include_tags,
        'exclude_tags': execution.exclude_tags,
        'limit': execution.limit
    }
