# -*- coding: utf-8 -*-

import json

import tornado.web


class Resource(tornado.web.RequestHandler):
    def data_received(self, chunk):
        raise NotImplemented  # Prevent static analysis errors about abstracts.

    def respond_json(self, data):
        self.set_header('Content-Type', 'application/json')
        self.write(data if isinstance(data, str) else json.dumps(data))

    def write_error(self, status_code, **kwargs):
        self.respond_json({
            'code': status_code,
            'message': self._reason
        })
