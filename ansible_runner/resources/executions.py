from ansible_runner.resources import Resource
import ansible_runner.representations.executions as executions_repr
from ansible_runner.services import executions_repo


class ExecutionsCollection(Resource):
    def post(self):
        new_execution = executions_repr.parse_json(self.request.body)
        new_execution = executions_repo.save(new_execution)
        self.respond_json(executions_repr.build_json(new_execution))
