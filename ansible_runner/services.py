# This is basically a stand-in for a service container. Since we're faking this
# part of the application for now, implementations will also be here.

import uuid

from ansible_runner.executions import (
    ExecutionsRepository, ExecutionList, Execution
)


class ExecutionsMemoryRepository(ExecutionsRepository):
    def __init__(self):
        self.executions = {}

    def find_all(self) -> ExecutionList:
        return [Execution(**item) for item in self.executions.values()]

    def save(self, execution: Execution) -> Execution:
        if execution.id is None:
            execution.id = str(uuid.uuid4())
        self.executions[execution.id] = {
            'id': execution.id,
            'status': execution.status,
            'playbook': execution.playbook,
            'include_tags': execution.include_tags,
            'exclude_tags': execution.exclude_tags,
            'limit': execution.limit
        }
        return execution


executions_repo = ExecutionsMemoryRepository()
