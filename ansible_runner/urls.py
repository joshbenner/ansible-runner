# -*- coding: utf-8 -*-

from ansible_runner.resources import executions


url_patterns = [
    (r'/executions', executions.ExecutionsCollection),
]
