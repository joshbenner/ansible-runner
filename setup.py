#!/usr/bin/env python

# -*- coding: utf-8 -*-

import os.path

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


exec(open('ansible_runner/version.py').read())

classifiers = """
'Development Status :: 2 - Alpha',
'Intended Audience :: Developers',
'License :: OSI Approved :: MIT License',
'Natural Language :: English',
'Programming Language :: Python :: 3',
'Programming Language :: Python :: 3.6',
"""

tests_require = [
    'coverage',
    'flake8',
    'pydocstyle',
    'pylint',
    'pytest-pep8',
    'pytest-cov',
    # for pytest-runner to work, it is important that pytest comes last in
    # this list: https://github.com/pytest-dev/pytest-runner/issues/11
    'pytest'
]

setup(
    name='ansible-runner',
    use_scm_version=True,
    description='Trigger Ansible runs via REST API.',
    long_description=read('README.rst'),
    author='Josh Benner',
    author_email='josh@bennerweb.com',
    url='https://gitlab.com/joshbenner/ansible-runner',
    classifiers=[c.strip() for c in classifiers.splitlines()
                 if c.strip() and not c.startswith('#')],
    include_package_data=True,
    packages=[
        'ansible_runner',
    ],
    test_suite='tests',
    setup_requires=['pytest-runner', 'setuptools_scm'],
    tests_require=tests_require
)
